//
//  ViewController.swift
//  table view
//
//  Created by set it solution on 02/04/2016.
//  Copyright © 2016 set it solution. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    
    var shipments: [String] = ["box", "cup", "fruit", ]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

extension ViewController : UITableViewDataSource, UITableViewDelegate {
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.shipments.count
    }

    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("Cell")!
        cell.textLabel?.text = self.shipments[indexPath.row]
        cell.detailTextLabel?.text = self.shipments[indexPath.row]
        return cell
    }
}